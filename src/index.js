import React from 'react';
import PropTypes from 'prop-types';

export default class LiveChatWrapper extends React.Component {
  componentWillMount() {
    this.loadLiveChatApi.bind(this)();
  }

  chatLoaded() {
    if ( !window.purechatApi.done
      && (
        !window.readyState
        || window.readyState == 'loaded'
        || window.readyState == 'complete'
      )
    ) {
      var w = new window.PCWidget({
        c: this.props.license,
        f: true
      });
      window.purechatApi.done = true;
      window.w = w;
    }
  }

  chatNotLoaded() {
    if (typeof this.props.onErrorLoading === 'function') {
      this.props.onErrorLoading();
    }
  }

  loadLiveChatApi() {
    if (!window.purechatApi) {
      window.purechatApi = window.purechatApi || {};
      window.purechatApi.done = false;
      window.purechatApi.l = [];
      window.purechatApi.t = [];
      window.purechatApi.on = () => this.l.push(arguments);
      // window.__lc.params = this.props.params;
      // window.__lc.visitor = this.props.visitor;

      const apiScript = document.createElement('script');
      apiScript.type = 'text/javascript';
      apiScript.async = true;
      apiScript.src = (
          'https:' === document.location.protocol ? 'https://' : 'http://'
        )
        + 'app.purechat.com/VisitorWidget/WidgetScript';

      const firstScript = document.getElementsByTagName('script')[0];
      firstScript.parentNode.insertBefore(apiScript, firstScript);
      apiScript.addEventListener('load', this.chatLoaded.bind(this));
      apiScript.addEventListener('error', this.chatNotLoaded.bind(this));
    }
  }

  render() {
    return null;
  }

  setCallbacks() {
    if (typeof this.props.onBeforeLoad === 'function')
      window.LC_API.on_before_load = this.props.onBeforeLoad.bind(this);

    if (typeof this.props.onAfterLoad === 'function')
      window.LC_API.on_after_load = this.props.onAfterLoad.bind(this);

    if (typeof this.props.onChatWindowOpened === 'function')
      window.LC_API.on_chat_window_opened = this.props.onChatWindowOpened.bind(this);

    if (typeof this.props.onChatWindowMinimized === 'function')
      window.LC_API.on_chat_window_minimized = this.props.onChatWindowMinimized.bind(this);

    if (typeof this.props.onChatWindowHidden === 'function')
      window.LC_API.on_chat_window_hidden = this.props.onChatWindowHidden.bind(this);

    if (typeof this.props.onChatStateChanged === 'function')
      window.LC_API.on_chat_state_changed = this.props.onChatStateChanged.bind(this);

    if (typeof this.props.onChatStarted === 'function')
      window.LC_API.on_chat_started = this.props.onChatStarted.bind(this);

    if (typeof this.props.onChatEnded === 'function')
      window.LC_API.on_chat_ended = this.props.onChatEnded.bind(this);

    if (typeof this.props.onMessage === 'function')
      window.LC_API.on_message = this.props.onMessage.bind(this);

    if (typeof this.props.onTicketCreated === 'function')
      window.LC_API.on_ticket_created = this.props.onTicketCreated.bind(this);

    if (typeof this.props.onPrechatSurveySubmitted === 'function')
      window.LC_API.on_prechat_survey_submitted = this.props.onPrechatSurveySubmitted.bind(this);

    if (typeof this.props.onRatingSubmitted === 'function')
      window.LC_API.on_rating_submitted = this.props.onRatingSubmitted.bind(this);

    if (typeof this.props.onRatingCommentSubmitted === 'function')
      window.LC_API.on_rating_comment_submitted = this.props.onRatingCommentSubmitted.bind(this);
  }
}

LiveChatWrapper.propTypes = {
  // important
  license: PropTypes.string.isRequired,
  group: PropTypes.number,
  onChatLoaded: PropTypes.func,
  // less important
  params: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired
  })),
  visitor: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string
  }),
  chatBetweenGroups: PropTypes.bool,
  onBeforeLoad: PropTypes.func,
  onAfterLoad: PropTypes.func,
  onChatWindowOpened: PropTypes.func,
  onChatWindowMinimized: PropTypes.func,
  onChatWindowHidden: PropTypes.func,
  onChatStateChanged: PropTypes.func,
  onChatStarted: PropTypes.func,
  onChatEnded: PropTypes.func,
  onMessage: PropTypes.func,
  onTicketCreated: PropTypes.func,
  onPrechatSurveySubmitted: PropTypes.func,
  onPostchatSurveySubmitted: PropTypes.func,
  onRatingSubmitted: PropTypes.func,
  onRatingCommentSubmitted: PropTypes.func,
};

LiveChatWrapper.defaultProps = {
  group: 0,
};
